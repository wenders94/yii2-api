<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%all_user}}`.
 */
class m200802_133259_create_all_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%all_user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'username',
            'all_user',
            'username'
        );
        $this->createIndex(
            'email',
            'all_user',
            'email'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%all_user}}');
    }
}
