<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;

class UserSearch extends User
{
    /**
     * @return array[]
     */
    public function rules()
    {
        return [
            [['username'], 'safe'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = parent::find();

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'username'
                    ]
                ],
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]
        );

        if (!$this->load($params)) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'username', $this->username]);

        return $dataProvider;
    }
}