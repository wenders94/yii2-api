<?php

namespace frontend\models;

use common\models\User as BaseUser;


class User extends BaseUser
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%all_user}}';
    }
}