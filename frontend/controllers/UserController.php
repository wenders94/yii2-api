<?php


namespace frontend\controllers;

use frontend\models\User;
use frontend\models\UserSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\ErrorAction;

class UserController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $users = new UserSearch();
        $dataProvider = $users->search(Yii::$app->request->get());
        $dataUsers = [
            'usersDataProvider' => $dataProvider,
            'usersFilterModel' => $users
        ];

        return $this->render('index.twig', $dataUsers);
    }
}