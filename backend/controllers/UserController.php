<?php


namespace backend\controllers;

use backend\actions\userController\CreateAction;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use backend\models\rest\User;
use yii\rest\Serializer;
use yii\web\Response;

class UserController extends ActiveController
{
    public $modelClass = User::class;

    /**
     * @var array
     */
    public $serializer = [
        'class' => Serializer::class,
        'collectionEnvelope' => 'items',
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::class,
                'formatParam' => '_format',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                    'xml' => Response::FORMAT_XML
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['create']['class'] = CreateAction::class;
        return $actions;
    }
}