<?php

namespace backend\jobs;

use yii\base\BaseObject;
use yii\queue\JobInterface;
use yii\queue\Queue;
use backend\models\rest\User;

class AddToUser extends BaseObject implements JobInterface
{
    public $username;
    public $email;

    /**
     * @param Queue $queue
     * @return mixed|void
     */
    public function execute($queue)
    {
        $user = new User();
        $data = [
            'username' => $this->username,
            'email' => $this->email
        ];
        $user->load($data, '');
        $user->save();
    }
}