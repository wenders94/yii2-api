<?php


namespace backend\actions\userController;

use backend\jobs\AddToUser;
use Yii;
use yii\web\ServerErrorHttpException;

class CreateAction extends \yii\rest\CreateAction
{
    /**
     * @return string|\yii\db\ActiveRecord|\yii\db\ActiveRecordInterface
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $request = Yii::$app->getRequest()->getBodyParams();
        $jobId = Yii::$app->queue->delay(10)->push(new AddToUser($request));

        if (Yii::$app->queue->isWaiting($jobId)) {
            return 'Request added to queue';
        } else {
            throw new ServerErrorHttpException('Failed adding to queue.');
        }
    }
}