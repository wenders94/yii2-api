<?php

use common\models\User;
use yii\log\FileTarget;
use yii\rest\UrlRule;
use yii\web\JsonParser;
use yii\queue\redis\Queue;
use yii\redis\Cache;
use yii\redis\Connection;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'name' => 'API Users',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', 'queue'],
    'modules' => [],
    'components' => [
        'cache' => [
            'class' => Cache::class,
        ],
        'redis' => [
            'class' => Connection::class,
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0
        ],
        'queue' => [
            'class' => Queue::class,
            'redis' => 'redis',
            'channel' => 'queue',
            'ttr' => 5 * 60,
            'attempts' => 3
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
            'parsers' => [
                'application/json' => JsonParser::class,
            ]
        ],
        'user' => [
            'identityClass' => User::class,
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'pattern' => '/',
                    'route' => 'user/index',
                    'suffix' => '/',
                ]
            ],
        ],
    ],
    'params' => $params,
];
