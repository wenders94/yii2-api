<?php

namespace backend\models\rest;

use common\models\User as BaseUser;

class User extends BaseUser
{
    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'username',
            'email'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%all_user}}';
    }
}